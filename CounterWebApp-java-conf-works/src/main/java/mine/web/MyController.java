package mine.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.logging.Level;
import java.util.logging.Logger;
import mine.data.data;


@Controller
public class MyController {
    private final static Logger LOGGER = Logger.getLogger(MyController.class.getCanonicalName());

    @RequestMapping({"/", "/here"})
    public String hello(final data data, ModelMap model) {
	//LOGGER.log(Level.INFO, "MyController ");
	LOGGER.log(Level.INFO, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	LOGGER.log(Level.INFO, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> data0={0}", new Object[]{data.getData0()});
	LOGGER.log(Level.INFO, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	model.addAttribute("message", "Hello");
        return "index";
    }
}
