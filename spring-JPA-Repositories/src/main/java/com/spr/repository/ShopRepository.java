package com.spr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

// ERH
import java.util.List;

import com.spr.model.Shop;

public interface ShopRepository extends JpaRepository<Shop, Integer> {
    // ERH
    List<Shop> findByName(String name);
}
