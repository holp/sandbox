package com.tutorialspoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class MainApp {

	@Autowired
	private SpellChecker spellChecker;
	
	void t(){
		spellChecker.checkSpelling();
	}
	
	public static void main(String[] args) {
		
		//MainApp m = new MainApp();
		//m.t();
		
		//spellChecker.checkSpelling();
		
		/*
	   ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

	   TextEditor te = (TextEditor) context.getBean("textEditor");

	   te.spellCheck();
	   */
		
		
		TextEditor te = new TextEditor();
		te.spellCheck();
	}

}
