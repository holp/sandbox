package mine.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class MyController {
    private final static Logger LOGGER = Logger.getLogger(MyController.class.getCanonicalName());

    @SuppressWarnings("unused")
    private static final String rcsinfo = "$Id: MyController.java,v 1.7 2016/04/27 03:58:44 ericholp Exp $";

    // ------------------------------------------------------------------------
    @RequestMapping({"/"})
    public String Top() {
	LOGGER.log(Level.INFO, "Top");
        return "index";
    }

    // ------------------------------------------------------------------------
    @ResponseBody
    @RequestMapping("/Record")
    public String Record(
			 @RequestParam(value="last", required=false) String last,
			 @RequestParam(value="first", required=false) String first,
			 @RequestParam(value="middle", required=false) String middle,
			 @RequestParam(value="pid", required=false) String pid
			 ) {
	LOGGER.log(Level.INFO, "Record: last={0} first={1} middle={2} pid={3}", new Object[]{last, first, middle, pid});

	// data: { last: last, first: first, middle: middle, pid: pid }

	// set required=true?
	if(last == null && first == null && middle == null && pid == null){
	    return "";
	}

	return "server: " + last + ", " + first + ", " + middle + ", " + pid;
    }

}
