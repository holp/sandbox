
// $Id: cardreader.js,v 1.3 2016/04/27 04:32:09 ericholp Exp $

var hc="%B6009050000000000^Clinton/Hillary Diane Rodh^0000000Z11111111800000000000000?;6009050000000000=00000002640507411800?";
var bs="%B6009050000000000^Sanders/Bernard           ^0000000A23456789800000000000000?;6009050000000000=00000002640507411800?";

time=setInterval(function(){
    try{
	var swipe=$('#swipe').val();
	var l = swipe.length;
	if(l >= 116){
	    $('#swipe').val("");
	    if(valid(swipe)){
		var namestr=swipe.substr(19, 26);
		var pid=swipe.substr(53, 61-53);
		$('#namestr').html(namestr);
		$('#pid').html(pid);
		var vals=parsename(namestr);
		$('#last').html("last: " + vals[0]);
		$('#first').html("first: " + vals[1]);
		$('#middle').html("middle: " + vals[2]);
		$('#status').html("");
		
		$.ajax({
		    method: "POST",
		    url: "/cardreader/Record",
		    data: { last: vals[0], first: vals[1], middle: vals[2], pid: pid }
		}).done(function( response ){
		    $('#response').html(response);
		});
		
	    }else{
		$('#status').html("swipe invalid");
		$('#namestr').html("");
		$('#pid').html("");
		$('#last').html("");
		$('#first').html("");
		$('#middle').html("");
		$('#status').html("");
		$('#response').html("");
	    }
	}
    }catch(ex){
	alert("loop: " + ex);
    }
},500);

function valid(swipe){
    try{
	var patt = /^%B6009050000000000\^[A-Za-z\/ ]{26}\^0000000[AZ][0-9]{8}800000000000000\?\;6009050000000000\=00000002640507411800\?$/;
	if(patt.test(swipe)){
	    return 1;
	}
	return 0;
    }catch(ex){
	alert("valid: " + ex);
	return 0;
    }
}

function inject(swipe){
    try{
	$('#swipe').val(swipe);
    }catch(ex){
	alert("inject: " + ex);
    }
}

function parsename(namestr){
    try{
	var re = /(.*?)\/(.*?) (.*)/;
	var match = re.exec(namestr);
	var last=match[1];
	var first=match[2];
	var middle=match[3];
	return [last, first, middle];
    }catch(ex){
	alert("parsename: " + ex);
	return [];
    }
}
