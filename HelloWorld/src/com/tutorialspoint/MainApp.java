package com.tutorialspoint;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;

//import javax.sql.DataSource;

//@Repository
public class MainApp {
	
	@Autowired 
	static JdbcTemplate jdbcTemplate;
	
	@Autowired
	static HelloWorld helloWorld;

	/*
    @Autowired
    public void setDataSource(DataSource dataSource) {
    	System.out.println("datasource=" + dataSource);
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    */
	
	/*
	public JdbcTemplate getJdbcTemplate( ){
		   return jdbcTemplate;
	}
	*/
	
	public static void main(String[] args) {
      
	
	  ApplicationContext context = 
	             new ClassPathXmlApplicationContext("Beans.xml");

	  
	  //jdbcTemplate = (JdbcTemplate) context.getBean("jdbcTemplate");
	  
	  
	  
	  //DataSource dataSource = (DataSource) context.getBean("dataSource");
	  
	  
	  //HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
	  helloWorld = (HelloWorld) context.getBean("helloWorld");

	  //obj.getMessage();
	  helloWorld.getMessage();
		
		System.out.println(">" + jdbcTemplate + "<");
	  
	  String stmnt = "select field from test where id = ?";
	  String field = 
			  jdbcTemplate.queryForObject(
					  stmnt, 
					  new Object[]{11}, 
					  String.class);
	  
	  System.out.println(field);
	  
	  
	  
	}
}
