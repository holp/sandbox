package mine.entity;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
//import org.springframework.dao.DataAccessException;

@Component
public class Main {
	@Autowired
	static JdbcTemplate template;
	  
	public static void main(String[] args) {
		
		String stmnt = "select field from test where id = ?";
	
		String field = template.queryForObject(
				stmnt, 
				new Object[]{11}, 
				String.class);
	
		System.out.println(field);
	}
}
