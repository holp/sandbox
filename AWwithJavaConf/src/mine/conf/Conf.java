package mine.conf;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.context.annotation.ComponentScan;

@Configuration
@ComponentScan("mine.entity")
public class Conf {
	@Value("${mine.username}")
	String username;
	@Value("${mine.password}")
	String password;
	@Value("${mine.url}")
	String url;
	@Value("${mine.driver:net.sourceforge.jtds.jdbc.Driver}")
	String driver;

	@Bean
	DataSource datasource() {
		System.out.println("conf datasource");
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setUsername(username);
		ds.setPassword(password);
		ds.setUrl(url);
		ds.setDriverClassName(driver);
		return ds;
	}

	@Bean
	JdbcTemplate template() {
		System.out.println("conf template");
		return new JdbcTemplate(datasource());
	}
}
