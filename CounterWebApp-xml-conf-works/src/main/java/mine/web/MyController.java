package mine.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.logging.Level;
import java.util.logging.Logger;


@Controller
public class MyController {
    private final static Logger LOGGER = Logger.getLogger(MyController.class.getCanonicalName());

    @RequestMapping("/")
    public String hello(ModelMap model) {
	LOGGER.log(Level.INFO, "MyController");
	model.addAttribute("message", "Hello");
        return "index";
	//return "WEB-INF/templates/index.html";
    }
}
